const needle = require('needle');
const cheerio = require('cheerio');

class ETSYParser {
    parsePage(url, callback) {

        //set http-client parameters
        needle.defaults({
            open_timeout: 60000,
            user_agent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
        });

        //get page and parse products
        needle.get(url, function(err, res) {
            if (err) throw err;

            const $ = cheerio.load(res.body, { decodeEntities: false } );

            const products = [];

            $('ul#reorderable-listing-results li').each((a, item) => {
                const product = {
                    title:   $(item).find("div.v2-listing-card__info h2").text().trim(),
                    url:     $(item).find("a.listing-link").attr("href"),
                    img:     $(item).find("div.v2-listing-card__img img").attr("src"),
                    company: $(item).find('span.v2-listing-card__shop span:first-child').html(),
                    rating:  $(item).find('span.v2-listing-card__rating input[name=rating]').val(),
                    orders:  $(item).find('span.v2-listing-card__rating span.icon-b-1').html(),
                    price:   parseFloat($(item).find('span.currency-value:first-child').text().replace(/,/g,'.')),
                };

                if ( product.orders != null ) {
                    product.orders = product.orders.replace(/\(|\)/g,'').trim();
                }
                products.push(product);
            });

            callback ( products );

        });
    };
};


module.exports = ETSYParser;
