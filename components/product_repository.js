


class ProductRepository {  
  constructor(dao) {
    this.dao = dao
  }

  createTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS products (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      title TEXT,
      url TEXT,
      company TEXT,
      rating TEXT,
      orders TEXT,
      price TEXT,
      img BLOB
    )`
    return this.dao.run(sql)
  }

  insert(product) {

    const sql = 'INSERT INTO products ('+Object.keys(product).join(",")+') VALUES (?' + (",?".repeat(Object.keys(product).length-1)) +')';
    return this.dao.run(sql, Object.values(product));
  }

  update(product) {
    const { id, title } = product
    return this.dao.run(
      `UPDATE products SET title = ? WHERE id = ?`,
      [title, id]
    )
  }

  delete(id) {
    return this.dao.run(
      `DELETE FROM products WHERE id = ?`,
      [id]
    )
  }
  
  getById(id) {
    return this.dao.get(
      `SELECT * FROM products WHERE id = ?`,
      [id])
  }

  getAll() {
    return this.dao.all(`SELECT * FROM products`)
  }
}

module.exports = ProductRepository; 

