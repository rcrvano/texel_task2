const Promise = require('bluebird')  
const AppDAO = require('./dao')  
const ProductRepository = require('./product_repository')  

class ProductDAO {
    constructor() {
        this.dao = new AppDAO('./database.sqlite3')
        this.productRepo = new ProductRepository(this.dao)
        this.productRepo.createTable()
           .catch((err) => {
              console.log('Error: ')
              console.log(JSON.stringify(err))
            })

    }

    add (product) {

       return this.productRepo.insert(product)
       .catch((err) => {
          console.log('Error: ')
          console.log(JSON.stringify(err))
        })

    };

    getAll() {
       return this.productRepo.getAll()
       .catch((err) => {
          console.log('Error: ')
          console.log(JSON.stringify(err))
        })
    }

};

module.exports = ProductDAO  

