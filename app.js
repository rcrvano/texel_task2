const ETSYParser = require("./parsers/etsy");
const ProductDAO = require("./components/product");

const express = require("express");
const fs = require('fs');
const http = require('http');
const path = require('path');
 
const app = express();
app.set('port', 3001);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
 
// Обслуживание статических ресурсов
app.get('/', function(req, res) {
  fs.readFile('./public/index.html', function(err, data) {
    res.end(data);
  });
});
 
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});






const parser = new ETSYParser();
const productDAO = new ProductDAO();



app.get('/all', function(req, res) {

    productDAO.getAll().
        then( (products) => { 
            res.send(products);
            res.end();
        });
});

app.post('/parse/etsy', function(req, res) {

    const url = req.body.url;
    console.log(url);
    if ( url === "" ) 
    {
        res.send([]);
        res.end();
        return;
    }
    console.log(1);
    //res.end(`Fetched and stored in SQLite3 database 0 products`);
    parser.parsePage(req.body.url, (products) => {
        if ( products.length ) {
            products.forEach( (product) => productDAO.add(product) );
        };


        res.send(products);
        res.end();

//        res.end(`Fetched and stored in SQLite3 database ${products.length} products`);
        //res.end(`Fetched ${products.length} products`);
    });
});


