Task 2. “Time for shopping!”
---------------------------

Sometimes we all need to choose and buy something online, but we all know how time consuming it is - let’s try to automate it in some way. Your task is to implement a little helper using any language that does the following:

- Parse an online shop’s catalog (any shop you like)

- Identify products you need based on any criteria you can come up with (e.g. size, price, color, type, etc.)

- Store results in an SQLite database (e.g. id, title, price, link to the product’s page)

- Optionally save product images

Requirements:

- You can use any publicly available API or library

- Parsing should be performed on HTML page and not on machine-readable format
