$(document).ready(function() {
    $('#url').keydown(function(e) {
      if(e.keyCode == 13){ //Enter pressed
        fetchURL();
      };
    });

    $('.fetch-button').on('click', function(e) {
        fetchURL();
        e.preventDefault()
    });


    function fetchURL () {
        const url = $('#url').val();
        $.preloader.start({src : '/img/sprites1.png'});


        $.ajax({
          type: "POST",
          url: "/parse/etsy",
          data: JSON.stringify({"url" : url.trim()}),
          contentType: "application/json",
          success: function(response, statusText, jqXHR) {
              if(jqXHR.status == 200) {
                  $.preloader.stop();
                  console.log(response);
                  $('#fetched-count').html( 'Fetched and stored in SQLite3 database '+response.length+' products' );
                  loadTableData(response);
              };
          }  
        });
    }


    function fetchAll () {
        $.preloader.start({src : '/img/sprites1.png'});

        $.get("/all",{
          contentType: "application/json"
        }).done( function(response, statusText, jqXHR) {
          if(jqXHR.status == 200) {
              $.preloader.stop();
              $('#fetched-count').html( 'In database stored '+response.length+' products' );
              loadTableData(response);
          };
        });
    }


    var counter = 1;
    function loadTableData(products)
    {
        var res_products = [];
        products.forEach ( function(element) {
            element.id=counter++;
            element.img_html   = "<img width=100 src=" + element.img + "/>";
            element.title_html = "<a href='" + element.url + "'>" + element.title + "</a>";
            element.price_html = '$' + element.price;
            res_products.push(element);
        });

        console.log(res_products);


        $('#table').bootstrapTable('removeAll');
        $('#table').bootstrapTable({
              columns: [{
                field: 'title_html',
                title: 'Titile'
              }, {
                field: 'img_html',
                title: 'Image'
              }, {
                field: 'price_html',
                title: 'Item Price'
              },
              {
                field: 'company',
                title: 'Company'
              },
              {
                field: 'rating',
                title: 'Rating'
              },
              {
                field: 'orders',
                title: 'Orders'
              },
              ],
        });
        $('#table').bootstrapTable('load',res_products);

              //data: res_products
    }

    
    fetchAll();
});
